<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully install the Yii-powered Test application.</p>

        <p><a class="btn btn-lg btn-success" href="index.php?r=site/subscribe">Get started with Test Application</a></p>
    </div>

    <div class="row">
        <div class="col-xs-3">
            <p>
                <strong>Author:</strong><br/>
                Eugene Samoylov<br/>

            </p>
        </div>

        <div class="col-xs-3">
            <p>
                <strong>Contacts:</strong><br/>
                <a href="tel:+79260692902">+79260692902</a><br/>
                <a href="mailto:fecce91@gmail.com">fecce91@gmail.com</a>
            </p>
        </div>

        <div class="col-xs-5 col-xs-offset-1">
            <p>
                <strong>Warning!</strong><br/>
                Yii framework (especially version 2) is a new for me. That`s why I have installed the basic template of
                the framework. Some places of this test application are not commented because they are a part that I
                wrote.
            </p>

            <p>
                But there is a positive side of this situation - I learned something new. This framework is quite
                different from Kohana or Laravel for example, but interesting. Also I was very glad to see that the
                basic template is using bootstrap.
            </p>

            <p>
                Also I found some bad sides of this framework. For example, basic template is very awful. There is no
                any Template Engine and views have a very bad coding style. But I did not have much time for rewriting
                these mistakes, so I worked in this conditions.
            </p>
        </div>
    </div>
</div>
