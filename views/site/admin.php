<?php

/* @var $this yii\web\View */

$this->title = 'Admin Panel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Welcome to Admin Panel!</h1>

        <p class="lead">You have successfully reach the end of the Test Application.</p>

        <p class="lead">In this place you can see all of the subscriptions.</p>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($subscriptions as $subscription): ?>
                        <tr>
                            <td><?= $subscription->name ?></td>
                            <td><?= $subscription->email ?></td>
                            <td><?= $subscription->created_at ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
