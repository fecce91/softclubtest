<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Subscribe';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-subscribe">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (isset($success)): ?>
        <div class="row">
            <div class="col-xs-12">
                <?php if ($success === true): ?>
                    <p class="text-success">You was succesfully subscribed</p>
                <?php else: ?>
                    <p class="text-danger">Ooops, something went wrong</p>
                <?php endif ?>
            </div>
        </div>
    <?php endif ?>

    <p>Please fill out the following fields to subscribe:</p>

    <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]
    ); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'email') ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Subscribe', ['class' => 'btn btn-primary', 'name' => 'subscribe-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
