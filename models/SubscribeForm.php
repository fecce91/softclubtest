<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Subscription;

/**
 * ContactForm is the model behind the contact form.
 */
class SubscribeForm extends Model
{
    public $name;
    public $email;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email'], 'required'],
            // email has to be a valid email address
            ['email', 'email']
        ];
    }

    /**
     * Saving subscription information to database
     * @return boolean whether the model passes validation
     */
    public function subscribe()
    {
        if ($this->validate()) {
            $model = new Subscription();

            $model->email = $this->email;
            $model->name = $this->name;

            $model->save();

            return true;
        }

        return false;
    }
}
