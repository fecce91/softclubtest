<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_094953_create_subscription_table extends Migration
{
    public function up()
    {
        $this->createTable('subscription', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);
    }

    public function down()
    {
        $this->dropTable('subscription');
    }
}
