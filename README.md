Yii 2 SoftClub Test
============================

INSTALLATION
------------

Just create an common config in apache, where document root is web directory. Do not forget to execute `php yii migrate`

CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```
