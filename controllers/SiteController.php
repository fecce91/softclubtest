<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\SubscribeForm;
use app\models\Subscription;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'admin'],
                'rules' => [
                    [
                        'actions' => ['logout', 'admin'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Action of first main App page
     * @return string Rendered result
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Action of Admin page
     * @return string Rendered result
     */
    public function actionAdmin()
    {
        $subscriptions = Subscription::find()
            ->all();

        return $this->render('admin', [
            'subscriptions' => $subscriptions
        ]);
    }

    /**
     * Action of subscribe form
     * @return string Rendered result
     */
    public function actionSubscribe() {
        $model = new SubscribeForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $success = $model->subscribe();

            return $this->render('subscribe', [
                'model' => $model,
                'success' => $success
            ]);
        } else {
            return $this->render('subscribe', [
                'model' => $model
            ]);
        }


    }

    /**
     * Action for authentication
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/site/admin']);
        }

        return $this->render('login', [
                'model' => $model,
            ]
        );
    }

    /**
     * Action for logout
     * @return string|\yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
